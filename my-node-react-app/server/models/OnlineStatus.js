
const mongoose = require('mongoose'); 

const onlineStatusSchema = new mongoose.Schema({
  user: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User', required: true 
},
  isOnline: { 
    type: Boolean, 
    default: false 
},
  lastOnline: { 
    type: Date 
},

});

const OnlineStatus = mongoose.model('OnlineStatus', onlineStatusSchema);

module.exports = OnlineStatus;
