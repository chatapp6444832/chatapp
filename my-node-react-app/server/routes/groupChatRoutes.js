const express = require('express');
const router = express.Router();
const groupchatController = require('../controllers/groupchatController');


router.post('/create', async (req, res) => {
  try {
    const newGroupChat = await groupchatController.createGroupChat(req.body);
    res.status(201).json(newGroupChat);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while creating the group chat' });
  }
});


router.post('/:groupId/add-members', async (req, res) => {
  try {
    const updatedGroupChat = await groupchatController.addMembersToGroupChat(req.params.groupId, req.body.members);
    res.status(200).json(updatedGroupChat);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while adding members to the group chat' });
  }
});


router.post('/:groupId/send-message', async (req, res) => {
  try {
    const message = await groupchatController.sendMessage(req.params.groupId, req.body.sender, req.body.text);
    res.status(201).json(message);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while sending the message' });
  }
});


router.get('/:groupId/history', async (req, res) => {
  try {
    const history = await groupchatController.getGroupChatHistory(req.params.groupId);
    res.status(200).json(history);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while fetching the group chat history' });
  }
});


router.delete('/:groupId', async (req, res) => {
  try {
    await groupchatController.deleteGroupChat(req.params.groupId);
    res.status(204).send();
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while deleting the group chat' });
  }
});

module.exports = router;
