const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../authentication.js");

// Route to register a new user
router.post('/register', async (req, res) => {
  try {
    
    const newUser = await userController.registerUser(req.body);
    
    res.status(201).json(newUser);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Route to log in a user
router.post('/login', async (req, res) => {
  try {
    const loggedInUser = await userController.loginUser(req.body);
    res.status(200).json(loggedInUser);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Route to get user profile
router.get("/details", auth.verify, async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    

    const userDetails = await userController.detailsUser(userData.id);
    res.send(userDetails);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});
















// Route to update user profile
router.put('/profile/:userId', async (req, res) => {
  try {
    const updatedProfile = await userController.updateUserProfile(req.params.userId, req.body);
    res.status(200).json(updatedProfile);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while updating user profile' });
  }
});

module.exports = router;
