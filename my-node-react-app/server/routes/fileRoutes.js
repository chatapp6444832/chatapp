const express = require('express');
const router = express.Router();
const fileController = require('../controllers/fileController');


router.post('/upload', async (req, res) => {
  try {
    const uploadedFile = await fileController.uploadFile(req.file); 
    res.status(201).json(uploadedFile);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while uploading the file' });
  }
});


router.get('/files', async (req, res) => {
  try {
    const files = await fileController.getUploadedFiles();
    res.status(200).json(files);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while fetching uploaded files' });
  }
});

module.exports = router;
