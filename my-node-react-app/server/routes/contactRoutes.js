const express = require('express');
const router = express.Router();
const contactController = require('../controllers/contactController');


router.get('/:userId/contacts', async (req, res) => {
  const { userId } = req.params;
  try {
    const contacts = await contactController.getContacts(userId);
    res.status(200).json(contacts);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});



router.get('/details/:contactId', async (req, res) => {
  const { contactId } = req.params;
  try {
    const contacts = await contactController.getContactDetails(contactId);
    res.status(200).json(contacts);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});


router.post('/details', async (req, res) => {
  const { contactName } = req.body;
  try {
    const contacts = await contactController.getContactDetailsByName(contactName);
    res.status(200).json(contacts);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});



router.post('/add', async (req, res) => {
  const { userId, contactName, contactEmail } = req.body;
  try {
    const newContact = await contactController.addContact(
      userId,
      contactName,
      contactEmail
    );
    res.status(201).json(newContact);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});


router.delete('/:userId/contacts/:contactId', async (req, res) => {
  const { userId, contactId } = req.params;
  try {
    await contactController.deleteContact(userId, contactId);
    res.status(200).json({ message: 'Contact removed successfully' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

module.exports = router;
