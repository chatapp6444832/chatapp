const express = require('express');
const router = express.Router();
const messagesController = require('../controllers/messagesController');


router.post('/send', async (req, res) => {
  
  
  try {
    const newMessage = await messagesController.sendMessage(req.body);
    console.log(newMessage)
    res.status(201).json(newMessage);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});


router.get('/history/:user1Id/:user2Id', async (req, res) => {
  const { user1Id, user2Id } = req.params;
  try {
    const chatHistory = await messagesController.getChatHistory(user1Id, user2Id);
    res.status(200).json(chatHistory);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});


router.post('/history', async (req, res) => {
  const { user1Id, contact } = req.body;
  
  try {
    const chatHistory = await messagesController.getChatHistoryWithContactUsername(user1Id, contact);
    res.status(200).json(chatHistory);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});



module.exports = router;
