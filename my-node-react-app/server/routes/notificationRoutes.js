const express = require('express');
const router = express.Router();
const Notification = require('../models/Notifications');
const socket = require('../socket'); 


router.post('/send', async (req, res) => {
  try {
  
    const { userId, message } = req.body;
    const newNotification = new Notification({ userId, message });
    const savedNotification = await newNotification.save();


    socket.emitToUser(userId, 'notification', savedNotification);

    res.status(201).json(savedNotification);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});


router.put('/:id/mark-read', async (req, res) => {
  try {
    const notificationId = req.params.id;

   
    const updatedNotification = await Notification.findByIdAndUpdate(
      notificationId,
      { read: true },
      { new: true }
    );

    res.json(updatedNotification);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

module.exports = router;
