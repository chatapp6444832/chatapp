const express = require('express');
const router = express.Router();
const Wysiwyg = require('../models/Wysiwyg');


router.post('/save', async (req, res) => {
  try {
    const { content } = req.body;

    const newWysiwygContent = new Wysiwyg({
      content,
    });

    const savedContent = await newWysiwygContent.save();
    res.status(201).json(savedContent);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});


router.get('/all', async (req, res) => {
  try {
    const contentList = await Wysiwyg.find();
    res.json(contentList);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

module.exports = router;
