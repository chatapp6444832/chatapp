const Message = require('../models/Messages');
const User = require('../models/User');

// SEND MESSAGE

module.exports.sendMessage = async (req, res) => {
  try {

    const { content } = req;

    const sender = await User.findOne({ username: req.senderUsername });
    const receiver = await User.findOne({ username: req.receiverUsername });   

    if (!sender || !receiver) {
        throw new Error('Sender or receiver not found');
      }

    const newMessage = await Message.create({
        senderId: sender._id,
        receiverId: receiver._id,
        content: content,
      
    });

    return newMessage;
  } catch (error) {
    throw new Error('An error occurred while sending the message');
  }
};


// GET MESSAGE

module.exports.getChatHistory = async (user1Id, user2Id) => {
  try {
    console.log ("this is the user1ID: " + user1Id);
    const chatHistory = await Message.find({
      $or: [
        { senderId: user1Id, receiverId: user2Id },
        { senderId: user2Id, receiverId: user1Id },
      ],
    }).sort({ createdAt: 1 });

    return chatHistory;
  } catch (error) {
    throw new Error('An error occurred while fetching chat history');
  }
};

// GET MESSAGE WITH CONTACT USERNAME

module.exports.getChatHistoryWithContactUsername = async (user1Id, contact) => {
  try {
    console.log ("this is the user1ID: " + user1Id);

    const contactInfo = await User.findOne({ username: contact });
    const chatHistory = await Message.find({
      $or: [
        { senderId: user1Id, receiverId: contactInfo._id },
        { senderId: contactInfo._id, receiverId: user1Id },
      ],
    }).sort({ createdAt: 1 });

    return chatHistory;
  } catch (error) {
    throw new Error('An error occurred while fetching chat history');
  }
};
