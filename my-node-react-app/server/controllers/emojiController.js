const Emoji = require('../models/Emoji'); 

module.exports.getAvailableEmojis = async () => {
  try {
    const emojis = await Emoji.find();

    return emojis;
  } catch (error) {
    throw new Error('An error occurred while retrieving available emojis');
  }
};
