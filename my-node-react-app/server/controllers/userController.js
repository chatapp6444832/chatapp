const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/User'); 
const auth = require("../authentication.js");


// REGISTER USER


module.exports.registerUser = async (req, res) => {
  try {
    
    const { username, email, password, avatarUrl } = req;
    
    const existingUser = await User.findOne({ username });
    
    if (existingUser) {
        throw new Error('Username already exists');
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    
    const newUser = await User.create({ username, email, password: hashedPassword, avatarUrl });
    
    return newUser;
  } catch (error) {
    throw error;
  }
};


// LOGIN USER


module.exports.loginUser = async (req, res) => {
  try {
    const { username, password } = req;

    const user = await User.findOne({ username });
    if (!user) {
        throw new Error('Username or password is incorrect');
    }

    const passwordMatch = await bcrypt.compareSync(password, user.password);
    if (!passwordMatch) {
        throw new Error('Username or password is incorrect');;
    }

    const token = {access: auth.createAccessToken(user)};

    return token;
  } catch (error) {
    throw error;
  }
};

// Get USER details

module.exports.detailsUser = async (userId) => {
  try {
    const user = await User.findById(userId);
    if (!user) {
      throw new Error('User not found');
    }
    return user;
  } catch (error) {
    throw new Error('An error occurred while fetching user details');
  }
};
