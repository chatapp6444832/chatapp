const Notification = require('../models/Notifications'); 
const io = require('../socket'); 

module.exports.sendNotification = async (userId, message) => {
  try {
    const newNotification = await Notification.create({ userId, message });

  
    io.getIO().to(userId).emit('notification', newNotification);

    return newNotification;
  } catch (error) {
    throw new Error('An error occurred while sending the notification');
  }
};

module.exports.markNotificationAsRead = async (notificationId) => {
  try {
    const notification = await Notification.findByIdAndUpdate(notificationId, { read: true });

    if (!notification) {
      throw new Error('Notification not found');
    }

    return notification;
  } catch (error) {
    throw new Error('An error occurred while marking the notification as read');
  }
};
