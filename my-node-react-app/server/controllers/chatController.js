const Chat = require('../models/ChatRoom'); 
const Message = require('../models/Messages'); 

module.exports.startChat = async (participants) => {
  try {
    const newChat = await Chat.create({ participants });
    return newChat;
  } catch (error) {
    throw new Error('An error occurred while starting a new chat');
  }
};

module.exports.sendMessage = async (userId, contactId, content) => {
  try {
   
    const newMessage = await Message.create({
      senderId: userId,
      receiverId: contactId,
      content: content
    });

  
    let chat = await Chat.findOne({
      participants: [userId, contactId]
    });

    if (!chat) {
      
      chat = await Chat.create({
        participants: [userId, contactId],
        messages: [newMessage._id]
      });
    } else {
      
      chat.messages.push(newMessage._id);
      await chat.save();
    }

    return newMessage;
  } catch (error) {
    throw new Error('An error occurred while sending a message');
  }
};



module.exports.getChatHistory = async (userId) => {
  try {
   
    const chatHistory = await Chat.find({
      participants: userId
    }).populate('participants', 'username'); 

    return chatHistory;
  } catch (error) {
    throw new Error('An error occurred while fetching chat history');
  }
};



// module.exports.markMessagesAsRead = async (req, res) => {
//   try {
//     const { chatId, userId } = req.body;

//     await Message.updateMany({ chatId, sender: { $ne: userId } }, { $set: { read: true } });

//     res.status(200).json({ message: 'Messages marked as read' });
//   } catch (error) {
//     throw new Error('An error occurred');
//   }
// };

// module.exports.deleteChat = async (req, res) => {
//   try {
//     const chatId = req.params.id;

//     await Chat.findByIdAndDelete(chatId);

//     res.status(200).json({ message: 'Chat deleted successfully' });
//   } catch (error) {
//     throw new Error('An error occurred');
//   }
// };
