const File = require('../models/File'); 

module.exports.uploadFile = async (userId, fileData) => {
  try {
    const newFile = await File.create({ userId, ...fileData });

    return newFile;
  } catch (error) {
    throw new Error('An error occurred while uploading the file');
  }
};

module.exports.getFilesByUser = async (userId) => {
  try {
    const files = await File.find({ userId });

    return files;
  } catch (error) {
    throw new Error('An error occurred while retrieving files');
  }
};

module.exports.getFileById = async (fileId) => {
  try {
    const file = await File.findById(fileId);

    if (!file) {
      throw new Error('File not found');
    }

    return file;
  } catch (error) {
    throw new Error('An error occurred while retrieving the file');
  }
};
